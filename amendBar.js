const folderNumber = 1030;
const startTemplate = 1196;
const templateNumber = 99;
const puppeteer = require("puppeteer");
const url = "http://192.168.0.224:50001";
const clickDelay = 600;

const amend = async (template) => {
  // login
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();
  await page.setViewport({
    width: 1920,
    height: 1080
  });
  await page.goto(`${url}/`, { waitUntil: "networkidle2" });
  await page.type(
    "#app-container > div > div > div > form > div:nth-child(1) > input",
    "SuperAdmin"
  );
  await page.type(
    "#app-container > div > div > div > form > div:nth-child(2) > input",
    "P@ssw0rd1"
  );
  await Promise.all([
    page.click("#app-container > div > div > div > form > button"),
    page.waitForNavigation({ waitUntil: "networkidle0" })
  ]);

  // loop over the templates
  
  // for (let i = 0; i < templateNumber; i++) {
    await page.goto(
      `${url}/template/folder/${folderNumber}/template/edit/${template}`,
      { waitUntil: "networkidle0" }
    );

    // wait til the page is ready
    await delay(2000);

    // select the bottom bar
    const length = await page.evaluate(() => {
      const all = document.querySelectorAll(
        "#root > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div"
      );
      return Array.from(all).length;
    });
    await page.click(
      `#root > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(${length})`
    );

    // actions after selection

    // add conditional above the bar
    await page.click("#ComponentTreeCreateParentComponentButton");
    await page.click("#ComponentTreeChangeComponentTypeButton");
    await delay(clickDelay);
    await page.click(
      "#root > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div.sc-htpNat.bRcPmT > div.btn-group.show > div > button:nth-child(2)"
    );
    await delay(clickDelay);
    // edit conditional
    await page.click(
      "#root > div > div > div:nth-child(2) > div:nth-child(2) > div.p-2 > div > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > button"
    );
    await delay(clickDelay);
    await page.click(
      "#root > div > div > div:nth-child(2) > div:nth-child(2) > div.p-2 > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div:nth-child(3) > div.mb-1 > button"
    );
    await delay(clickDelay);
    // click input
    await page.click(
      "#root > div > div > div:nth-child(2) > div:nth-child(2) > div.p-2 > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div:nth-child(3) > div.collapse.show > div > div.value-select-node > div:nth-child(1) > button"
    );
    await delay(clickDelay);
    // click Services
    await page.click(
      "#root > div > div > div:nth-child(2) > div:nth-child(2) > div.p-2 > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div:nth-child(3) > div.collapse.show > div > div.value-select-node > div.dropdown.show > div > button:nth-child(4)"
    );
    await delay(clickDelay);
    // click dropdown
    await page.click(
      "#root > div > div > div:nth-child(2) > div:nth-child(2) > div.p-2 > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div:nth-child(3) > div.collapse.show > div > div.value-select-node > div:nth-child(2) > button"
    );
    await delay(clickDelay);
    // select node var
    await page.click(
      "#root > div > div > div:nth-child(2) > div:nth-child(2) > div.p-2 > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div:nth-child(3) > div.collapse.show > div > div.value-select-node > div.dropdown.show > div > button:nth-child(2)"
    );
    await delay(clickDelay);
    // click dropdown
    await page.click(
      "#root > div > div > div:nth-child(2) > div:nth-child(2) > div.p-2 > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div:nth-child(3) > div.collapse.show > div > div.value-select-node > div:nth-child(3) > button"
    );
    await delay(clickDelay);
    // select sth
    await page.click(
      "#root > div > div > div:nth-child(2) > div:nth-child(2) > div.p-2 > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div:nth-child(3) > div.collapse.show > div > div.value-select-node > div.dropdown.show > div > button:nth-child(1)"
    );
    await delay(clickDelay);
    // add input field
    await page.click(
      "#root > div > div > div:nth-child(2) > div:nth-child(2) > div.p-2 > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div:nth-child(3) > div.collapse.show > div > div.value-select-node > div:nth-child(3) > div > div:nth-child(2) > button"
    );
    await delay(clickDelay);
    // select is equal to
    await page.click(
      "#root > div > div > div:nth-child(2) > div:nth-child(2) > div.p-2 > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div:nth-child(3) > div.collapse.show > div > div.value-select-node > div:nth-child(3) > div > div.dropdown.show > div > button:nth-child(3)"
    );
    await delay(clickDelay);
    // fill in input
    await page.type(
      "#root > div > div > div:nth-child(2) > div:nth-child(2) > div.p-2 > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div:nth-child(3) > div.collapse.show > div > div:nth-child(2) > div:nth-child(3) > div > input",
      "true"
    );
    await delay(clickDelay);


    // save
    await page.click(
      "#root > div > div > div:nth-child(1) > div:nth-child(3) > div:nth-child(3) > button"
    );

    // wait til the page is saved
    await delay(4000);
  // }

  await browser.close();
}

for (let i = 0; i<templateNumber; i++){
    setTimeout(async()=>{
        await amend(startTemplate+i)
    }, 0)
}

function delay(time) {
  return new Promise(function(resolve) {
    setTimeout(resolve, time);
  });
}
