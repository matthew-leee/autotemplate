const folderNumber = 1029;
const startTemplate = 507;
const templateNumber = 2;
const puppeteer = require("puppeteer");
const url = "http://192.168.0.224:50001";

(async () => {
    // login
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();
  await page.setViewport({
    width: 1920,
    height: 1080
  });
  await page.goto(`${url}/`, { waitUntil: "networkidle2" });
  await page.type(
    "#app-container > div > div > div > form > div:nth-child(1) > input",
    "SuperAdmin"
  );
  await page.type(
    "#app-container > div > div > div > form > div:nth-child(2) > input",
    "P@ssw0rd1"
  );
  await Promise.all([
    page.click("#app-container > div > div > div > form > button"),
    page.waitForNavigation({ waitUntil: "networkidle0" })
  ]);




    // loop over the templates
  for (let i = 0; i < templateNumber; i++) {
    await page.goto(
      `${url}/template/folder/${folderNumber}/template/edit/${startTemplate +
        i}`,
      { waitUntil: "networkidle0" }
    );

    // wait til the page is ready
    await delay(2000);

    // select the bottom bar
    const length = await page.evaluate(() => {
      const all = document.querySelectorAll("#root > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div")
      return Array.from(all).length;
    });
    await page.click(`#root > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(${length})`);

    // actions after selection
    await page.click("#ComponentTreeComponentRemoveButton");

    // save
    await page.click(
      "#root > div > div > div:nth-child(1) > div:nth-child(3) > div:nth-child(3) > button"
    );

    // wait til the page is saved
    await delay(5000);
  }

  await browser.close();
})();

function delay(time) {
  return new Promise(function(resolve) {
    setTimeout(resolve, time);
  });
}
