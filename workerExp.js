const { Worker } = require("worker_threads");
const os = require("os");
const hyperThreading = true;
const cpuCount = hyperThreading ? os.cpus().length * 2 : os.cpus().length;

const startTemplate = 1790;
const templateNumber = 99;

const promises = async () => {
  let overallArr = [];
  let allTemplates = () => {
    let arr = [];
    for (let i = 0; i < templateNumber; i++) {
      arr.push(startTemplate + i);
    }
    return arr;
  };
  const onePortion = Math.ceil(templateNumber / cpuCount);
  for (i = 0; i < cpuCount; i++) {
    overallArr.push(allTemplates().slice(i * onePortion, (i + 1) * onePortion));
  }
  return overallArr.map(async arr => {
    const result = await runService(arr);
    return result;
  });
};

function runService(workerData) {
  return new Promise((resolve, reject) => {
    const worker = new Worker("./worker.js", { workerData });
    worker.on("message", resolve);
    worker.on("error", reject);
    worker.on("exit", code => {
      if (code !== 0)
        reject(new Error(`Worker stopped with exit code ${code}`));
    });
  });
}

async function run() {
  const result = await promises();
  console.log(result);
}

run().catch(err => console.error(err));
